Blue Ghost entry test
=================
This is a small project for entrance test into company Blue Ghost. It allows any user connected to the application to
see, create, edit and delete contacts in a book of contacts.

Installation
------------
This project is using Nette framework and its packages. The best way to install needed packages is using Composer. If
you don't have Composer yet, download it on the [official website](https://getcomposer.org/).

- Go inside terminal into folder with cloned repo and install the required packages with the command
  ***composer update***
- Dont forget to make directories `temp/` and `log/` writable.

Setup
----------------
Direct your PHP engine into the /www directory. The simplest way is running this command in the root directory:
***php -S localhost:8000 -t www***
Then visit `http://localhost:8000` in your browser

Finally, you will need a MySQL database. Use the "db.sql" file to import the database with tables. After, go into folder
/config, copy file "local.dist.neon" and create from it your own "
local.neon" file and provide the connection info for DB.

And remember:


![image.png](./image.png)

