<?php

namespace App\Model;

use Nette;
use Nette\Database\Explorer;

class BaseModel
{
    use Nette\SmartObject;

    protected Explorer $database;

    public function __construct(Explorer $database)
    {
        $this->database = $database;
    }
}