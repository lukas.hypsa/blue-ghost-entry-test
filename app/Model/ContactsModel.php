<?php

namespace App\Model;

use Nette\Database\Table\ActiveRow;
use App\Utils\UrlGenerator;

final class ContactsModel extends BaseModel
{
    const CONTACTS_TABLE = 'contacts';

    const URL_ID_ATRIBUTE = 'url_id';
    const NAME_ATRIBUTE= 'name';

    const ALL_ATRIBUTE_NAMES = [
        "id",
        "name",
        "surname",
        "phone",
        "email",
        "note",
    ];

    public static function getAtributeNames(): array
    {
        return self::ALL_ATRIBUTE_NAMES;
    }

    public function getAllContacts(): array
    {
        return $this->database
            ->table(self::CONTACTS_TABLE)
            ->fetchAll();
    }

    public function getContact(int $id)
    {
        return $this->database
            ->table(self::CONTACTS_TABLE)
            ->get($id);
    }

    public function getContactByUrlId(string $urlId)
    {
        return $this->database
            ->table(self::CONTACTS_TABLE)
            ->where(self::URL_ID_ATRIBUTE, $urlId)
            ->fetch();
    }

    public function createContact($data): ActiveRow
    {
        $name = $data[self::NAME_ATRIBUTE];
        $urlId = UrlGenerator::createUrlIdFromName($name) . "-" . rand(0, 10000); // adding random int to distinguish 2 contacts with same name
        $data[self::URL_ID_ATRIBUTE] = $urlId;

        return $this->database
            ->table(self::CONTACTS_TABLE)
            ->insert($data);
    }

    public function editContact($data): ActiveRow
    {
        $contact = $this->getContact($data["id"]);

        if($contact->name !== $data[self::NAME_ATRIBUTE]) {
            $name = $data[self::NAME_ATRIBUTE];
            $urlId = UrlGenerator::createUrlIdFromName($name) . "-" . rand(0, 10000); // adding random int to distinguish 2 contacts with same name
            $data[self::URL_ID_ATRIBUTE] = $urlId;
        }

        $contact->update($data);
        $contact = $this->getContact($data["id"]);

        return $contact;
    }

    public function deleteContact(int $id): int
    {
        $count = $this->database
            ->table(SELF::CONTACTS_TABLE)
            ->where('id', $id)
            ->delete();

        return $count;
    }
}