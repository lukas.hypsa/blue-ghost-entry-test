<?php

declare(strict_types=1);

namespace App\Router;

use Nette;
use Nette\Application\Routers\RouteList;


final class RouterFactory
{
	use Nette\StaticClass;

	public static function createRouter(): RouteList
	{
		$router = new RouteList;

        $router->addRoute('', 'Contact:list');
        $router->addRoute('create', 'Contact:create');
        $router->addRoute('<urlid>', 'Contact:edit');
        // $router->addRoute('/delete/[<id>]', 'Contact:delete');

        $router->addRoute('<presenter>/<action>[/<id>]', 'Contact:list');

		return $router;
	}
}
