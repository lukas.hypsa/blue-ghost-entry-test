<?php

namespace App\Utils;

final class UrlGenerator
{
    private static function accent2ascii(string $str, string $charset = 'utf-8'): string
    {
        $str = htmlentities($str, ENT_NOQUOTES, $charset);

        $str = preg_replace('#&([A-za-z])(?:acute|cedil|caron|circ|grave|orn|ring|slash|th|tilde|uml);#', '\1', $str);
        $str = preg_replace('#&([A-za-z]{2})(?:lig);#', '\1', $str); // pour les ligatures e.g. '&oelig;'
        $str = preg_replace('#&[^;]+;#', '', $str); // supprime les autres caractères

        return $str;
    }

    public static function createUrlIdFromName($name) {
        $newUrl = self::accent2ascii($name);
        $newUrl = strtolower($newUrl);

        $newUrl = str_replace(' ', '-', $newUrl);
        $newUrl = str_replace('ř', 'r', $newUrl);
        $newUrl = str_replace('ť', 't', $newUrl);
        $newUrl = str_replace('ů', 'u', $newUrl);
        $newUrl = str_replace('ě', 'e', $newUrl);
        $newUrl = str_replace('ď', 'd', $newUrl);
        $newUrl = str_replace('ž', 'z', $newUrl);
        $newUrl = str_replace('ň', 'n', $newUrl);
        $newUrl = str_replace('č', 'c', $newUrl);

        return $newUrl;
    }
}