<?php

namespace App\Presenters;

use Nette\Application\UI\Form;
use App\Model\ContactsModel;
use App\Utils\UrlGenerator;

class ContactPresenter extends BasePresenter
{
    private ContactsModel $contactsModel;
    private UrlGenerator $urlGenerator;

    public function __construct(ContactsModel $contactsModel,
                                UrlGenerator $urlGenerator,
    )
    {
        $this->contactsModel = $contactsModel;
        $this->urlGenerator = $urlGenerator;
    }

    public function beforeRender()
    {
        $this->template->contactAtributes = $this->contactsModel::getAtributeNames();
    }

    public function renderList(): void
    {
        $this->template->contacts = $this->contactsModel->getAllContacts();
    }

    public function renderCreate(): void
    {
    }

    public function renderEdit(string $urlId): void
    {
        $contact = $this->contactsModel->getContactByUrlId($urlId);

        foreach ($this->contactsModel->getAtributeNames() as $atr) {
            $defs[$atr] = $contact->$atr;
        }

        $this['editForm']->setDefaults($defs);
    }

    private function createComponentPostForm($successMethod): Form
    {
        $form = new Form;

        $form->addHidden('id', 'ID:');

        $form->addText('name', 'Name:');
        $form->addText('surname', 'Surname:');
        $form->addText('phone', 'Phone:');

        $form->addEmail('email', 'Email:');
        $form->addTextArea('note', 'Note:');

        $form->addSubmit('submit', 'Submit');

        $form->onSuccess[] = [$this, $successMethod];

        return $form;
    }

    public function createComponentCreateForm(): Form
    {
        return $this->createComponentPostForm("createFormSucceeded");
    }

    public function createComponentEditForm(): Form
    {
        return $this->createComponentPostForm("editFormSucceeded");
    }

    public function createFormSucceeded(array $data): void
    {
        $post = $this->contactsModel->createContact($data);

        $this->flashMessage("Contact has been added successfully!", 'success');
        $this->redirect('Contact:list');
    }

    public function editFormSucceeded(array $data): void
    {
        $updatedCont = $this->contactsModel->editContact($data);

        $this->flashMessage("Contact has been edited successfully!", 'success');
        $this->redirect('Contact:edit', $updatedCont->url_id);
    }

    public function handleDelete($id) {
        $this->contactsModel->deleteContact($id);

        $this->flashMessage("Contact has been deleted!", 'success');
        $this->redirect('Contact:list');
    }
}